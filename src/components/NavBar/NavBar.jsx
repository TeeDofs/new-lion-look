import React from 'react';
import './NavBar.scss';
import Logo from '../images/lincolnlion.png';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { NavLink } from 'react-router-dom';

import classNames from 'classnames';

import {
  Collapse,
  Navbar,
  NavbarToggler,
  Nav,
  NavItem,
  Row,
  Container,
  Col
} from 'reactstrap';
import { map, addIndex, find, propEq } from 'ramda';

const findCurrentOption = (option, optionMenu) =>
  find(propEq('text', option))(optionMenu);

class NavBar extends React.Component {
  render() {
    const mappedIndex = addIndex(map);
    const {
      toggleNavOption,
      navOptionIsOpen,
      navBarOptions,
      setSelectedNavBarOption,
      selectedNavBarOption,
      setSelectedSubNavOption,
      selectedSubNavOption,
      clickedNavBarOption,
      setClickedNavBarOption
    } = this.props;
    return (
      <div>
        <Navbar className='topNavBar' light expand='md'>
          <NavLink to='/'>
            <div
              className='logoHolder'
              onClick={() => setClickedNavBarOption(null)}
            >
              <img src={Logo} className='logoImage' alt='Lincoln Logo' />
            </div>
          </NavLink>

          <NavbarToggler onClick={toggleNavOption} />
          <Collapse isOpen={navOptionIsOpen} navbar>
            <Nav className='ml-auto navHolder' navbar>
              {mappedIndex(
                (navBarOption, idx) => (
                  <NavItem key={idx}>
                    <NavLink
                      to={navBarOption.link}
                      className={classNames({
                        navBarOption: true,
                        lastNavBarOption: idx + 1 === navBarOptions.length,
                        activeNavBarOption:
                          navBarOption.text === selectedNavBarOption ||
                          navBarOption.text === clickedNavBarOption
                      })}
                      onClick={() => setClickedNavBarOption(navBarOption.text)}
                      onMouseEnter={() =>
                        setSelectedNavBarOption(navBarOption.text)
                      }
                    >
                      {navBarOption.text}
                      {'   '}
                      <FontAwesomeIcon icon={navBarOption.icon} />
                    </NavLink>
                  </NavItem>
                ),
                navBarOptions
              )}
            </Nav>
          </Collapse>
        </Navbar>
        {!selectedNavBarOption ? null : (
          <Container
            fluid
            className={classNames({
              subMenuHolder: true
            })}
          >
            <Container
              className='subMenu'
              onMouseLeave={() => {
                setSelectedNavBarOption(null);
              }}
            >
              <Row>
                {mappedIndex(
                  (subMenuOption, idx) => (
                    <Col key={idx} className='subNavColumn'>
                      <div className='subMenuOption'>
                        <div className='subMenuTitle'>
                          <h5>{subMenuOption.menuTitle}</h5>
                        </div>
                        <div className='subMenuItems'>
                          <ul>
                            {mappedIndex(
                              (subMenuItem, idx) => (
                                <li
                                  className={classNames({
                                    activeSubMenuItem:
                                      subMenuItem.subMenuTitle ===
                                      selectedSubNavOption,
                                    subMenuItem: true
                                  })}
                                  key={idx + subMenuItem.subMenuTitle}
                                  onClick={() => {
                                    setSelectedSubNavOption(
                                      subMenuItem.subMenuTitle
                                    );
                                  }}
                                >
                                  {subMenuItem.subMenuTitle}{' '}
                                  <FontAwesomeIcon
                                    icon='paw'
                                    className='subPawIcon'
                                  />
                                </li>
                              ),
                              subMenuOption.subMenu
                            )}
                          </ul>
                        </div>
                      </div>
                    </Col>
                  ),
                  findCurrentOption(selectedNavBarOption, navBarOptions).menu
                )}
              </Row>
            </Container>
          </Container>
        )}
      </div>
    );
  }
}

export default NavBar;
