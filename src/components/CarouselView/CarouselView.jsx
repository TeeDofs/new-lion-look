import React from 'react';
import './CarouselView.scss';
import { Carousel, CarouselItem, CarouselIndicators } from 'reactstrap';

import { map } from 'ramda';

class CarouselView extends React.Component {
  render() {
    const {
      activeCarouselIndex,
      carouselImages,
      onCarouselExited,
      onCarouselExiting,
      nextCarouselImage,
      previousCarouselImage,
      goToCarouselIndex
    } = this.props;

    const slides = map(image => {
      return (
        <CarouselItem
          onExiting={onCarouselExiting}
          onExited={onCarouselExited}
          key={image.src}
        >
          <img className='carouselImage' src={image.src} alt={image.altText} />
        </CarouselItem>
      );
    }, carouselImages);

    return (
      <Carousel
        activeIndex={activeCarouselIndex}
        next={nextCarouselImage}
        previous={previousCarouselImage}
      >
        <CarouselIndicators
          items={carouselImages}
          activeIndex={activeCarouselIndex}
          onClickHandler={goToCarouselIndex}
        />
        {slides}
      </Carousel>
    );
  }
}

export default CarouselView;
