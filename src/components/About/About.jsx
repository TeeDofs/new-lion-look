import React from 'react';
import './About.scss';
import { Container, Row, Col } from 'reactstrap';
import { find, propEq, map, addIndex } from 'ramda';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import classNames from 'classnames';
import CarouselView from '../CarouselView/CarouselView';

const findCurrentOption = (option, optionMenu) =>
  find(propEq('text', option))(optionMenu);

class About extends React.Component {
  constructor(props) {
    super(props);
    this.setHoveredAboutItem = this.setHoveredAboutItem.bind(this);
    this.state = {
      hoveredAboutItem: null
    };
  }

  setHoveredAboutItem(item) {
    this.setState({
      hoveredAboutItem: item
    });
  }
  render() {
    const mappedIndex = addIndex(map);
    const {
      activeCarouselIndex,
      onCarouselExited,
      onCarouselExiting,
      nextCarouselImage,
      previousCarouselImage,
      goToCarouselIndex,
      navBarOptions
    } = this.props;
    return (
      <div className='aboutMainBody'>
        <Container fluid>
          <Row>
            <Col sm={12} md={5}>
              <Container fluid className='aboutLU'>
                <Container fluid className='aboutLUText'>
                  <h2>About</h2>
                  <h3>Lincoln University</h3>
                  <Container className='orangeLine'></Container>
                </Container>
                <Container className='aboutLUDescription'>
                  <Container>
                    <p>
                      Lincoln University is the first HBCU to be established in
                      1854. It is a school rich in history and culture.
                    </p>
                  </Container>
                </Container>
              </Container>
            </Col>
            <Col sm={12} md={7}>
              <div className='carouselDiv'>
                <CarouselView
                  activeCarouselIndex={activeCarouselIndex}
                  carouselImages={
                    findCurrentOption('About', navBarOptions).carouselImages
                  }
                  onCarouselExited={onCarouselExited}
                  onCarouselExiting={onCarouselExiting}
                  nextCarouselImage={nextCarouselImage}
                  previousCarouselImage={previousCarouselImage}
                  goToCarouselIndex={goToCarouselIndex}
                />
              </div>
            </Col>
          </Row>
        </Container>
        <Container fluid className='aboutMenuHolder'>
          <Container className='aboutMenuTitle'>
            <h1>About Us</h1>
          </Container>
          <Container className='aboutMenu'>
            {mappedIndex(
              (menu, idx) => (
                <Container fluid key={idx} className='aboutMenuRow'>
                  <Row noGutters className='aboutRow'>
                    <Col>
                      <div className='aboutMenuItem aboutRowTitle'>
                        <h2>{menu.menuTitle.toUpperCase()}</h2>
                      </div>
                    </Col>
                    {mappedIndex(
                      (subMenu, idx) => (
                        <Col key={idx}>
                          <div
                            className='aboutMenuItemGroup'
                            onMouseEnter={() => {
                              this.setHoveredAboutItem(subMenu.subMenuTitle);
                            }}
                            onMouseLeave={() => {
                              this.setHoveredAboutItem(null);
                            }}
                          >
                            <div
                              className={classNames({
                                aboutMenuItem: true,
                                aboutMenuItemHover:
                                  this.state.hoveredAboutItem ===
                                  subMenu.subMenuTitle
                              })}
                            >
                              <h3>{subMenu.subMenuTitle.toUpperCase()}</h3>
                              <FontAwesomeIcon
                                icon={subMenu.icon}
                                className='menuIcon'
                              />
                            </div>
                            {this.state.hoveredAboutItem !==
                            subMenu.subMenuTitle ? null : (
                              <div
                                className={classNames({
                                  menuItemInfo: idx !== menu.subMenu.length - 1,
                                  menuItemInfoLast:
                                    idx === menu.subMenu.length - 1
                                })}
                              >
                                <Container className='aboutItemMoreLink'>
                                  <h4>More Links</h4>
                                  <ul>
                                    {mappedIndex(
                                      (subLink, idx) => (
                                        <li key={idx}>{subLink.text}</li>
                                      ),
                                      subMenu.subLinks
                                    )}
                                  </ul>
                                </Container>
                              </div>
                            )}
                          </div>
                        </Col>
                      ),
                      menu.subMenu
                    )}
                  </Row>
                </Container>
              ),
              findCurrentOption('About', navBarOptions).menu
            )}
          </Container>
        </Container>
      </div>
    );
  }
}

export default About;
