import React from 'react';
import './Admissions.scss';
import { Container, Row, Col } from 'reactstrap';
import { find, propEq, map, addIndex } from 'ramda';
import CarouselView from '../CarouselView/CarouselView';

const findCurrentOption = (option, optionMenu) =>
  find(propEq('text', option))(optionMenu);

class Admissions extends React.Component {
  render() {
    const mappedIndex = addIndex(map);
    const {
      activeCarouselIndex,
      navBarOptions,
      onCarouselExited,
      onCarouselExiting,
      nextCarouselImage,
      previousCarouselImage,
      goToCarouselIndex
    } = this.props;
    return (
      <div className='admissionsBody'>
        <Container fluid>
          <Row>
            <Col sm={12} md={5}>
              <Container fluid className='aboutLU'>
                <Container fluid className='aboutLUText'>
                  <h2>Admissions</h2>
                  <h3>Embracing Potential</h3>
                  <Container className='orangeLine'></Container>
                </Container>
                <Container className='aboutLUDescription'>
                  <Container>
                    <p>
                      Lincoln University is the first HBCU to be established in
                      1854. It is a school rich in history and culture.
                    </p>
                  </Container>
                </Container>
              </Container>
            </Col>
            <Col sm={12} md={7}>
              <div className='carouselDiv'>
                <CarouselView
                  activeCarouselIndex={activeCarouselIndex}
                  carouselImages={
                    findCurrentOption('Admissions', navBarOptions)
                      .carouselImages
                  }
                  onCarouselExited={onCarouselExited}
                  onCarouselExiting={onCarouselExiting}
                  nextCarouselImage={nextCarouselImage}
                  previousCarouselImage={previousCarouselImage}
                  goToCarouselIndex={goToCarouselIndex}
                />
              </div>
            </Col>
          </Row>
        </Container>
        <Container className='admissionsMenuHolder'>
          {mappedIndex(
            (option, idx) => (
              <Container className='admissionsMenuContainer' key={idx} fluid>
                <h3>{option.menuTitle}</h3>
              </Container>
            ),
            findCurrentOption('Admissions', navBarOptions).menu
          )}
        </Container>
      </div>
    );
  }
}
export default Admissions;
