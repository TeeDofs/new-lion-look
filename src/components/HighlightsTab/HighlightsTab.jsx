import React from 'react';
import './HighlightsTab.scss';
import {
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Row,
  Col,
  Container
} from 'reactstrap';
import classNames from 'classnames';
import { map, addIndex } from 'ramda';

const mappedIndex = addIndex(map);

class HighlightsTab extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: '1'
    };
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }
  render() {
    const {
      navTabHighlights,
      toggleNavTab,
      activeNavTab,
      navTabOurImpacts
    } = this.props;
    return (
      <div>
        <Nav tabs>
          <NavItem>
            <NavLink
              className={classNames({ active: activeNavTab === '1' })}
              onClick={() => {
                toggleNavTab('1');
              }}
            >
              <span className='navText'>Highlights</span>
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classNames({ active: activeNavTab === '2' })}
              onClick={() => {
                toggleNavTab('2');
              }}
            >
              <span className='navText'>Our Impacts</span>
            </NavLink>
          </NavItem>
        </Nav>
        <TabContent activeTab={activeNavTab}>
          <TabPane tabId='1'>
            <Row className='highlightTabRow'>
              {mappedIndex(
                (highlight, idx) => (
                  <Col key={idx} sm={2}>
                    <div className='highlightImageHolder'>
                      <img
                        className='highlightImage'
                        src={highlight.image}
                        alt={highlight.section}
                      />
                    </div>
                    <div className='highlightTextHolder'>
                      <h6>{highlight.section}</h6>
                    </div>
                  </Col>
                ),
                navTabHighlights
              )}
            </Row>
          </TabPane>
          <TabPane tabId='2'>
            <Container className='ourImpactContainer'>
              <Row className='highlightTabRow'>
                {mappedIndex(
                  (impact, idx) => (
                    <Col key={idx} sm={2}>
                      <div className='highlightImageHolder'>
                        <img
                          className='highlightImage'
                          src={impact.image}
                          alt={impact.section}
                        />
                      </div>
                      <div className='highlightTextHolder'>
                        <h6>{impact.section}</h6>
                      </div>
                    </Col>
                  ),
                  navTabOurImpacts
                )}
              </Row>
            </Container>
          </TabPane>
        </TabContent>
      </div>
    );
  }
}

export default HighlightsTab;
