import React from 'react';
import './PageFooter.scss';
import LincolnLogo from '../images/lincolnLogo.png';

import { Container, Row, Col } from 'reactstrap';

import { map, addIndex } from 'ramda';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const mappedIndex = addIndex(map);

class PageFooter extends React.Component {
  render() {
    const { socialMedia } = this.props;
    return (
      <Container className='footerContainer' fluid>
        <Container className='footerSubDiv'>
          <Row>
            <Col sm={2}>
              <ul className='footerListItems leftItemsList'>
                <li>Students</li>
                <li>Faculty</li>
                <li>Staff</li>
                <li>Parents</li>
                <li>Alumni</li>
              </ul>
            </Col>
            <Col sm={2}>
              <ul className='footerListItems leftItemsList'>
                <li>Directory</li>
                <li>Campus Notices</li>
                <li>Maps</li>
                <li>Quick Links</li>
                <li>Make a Donation</li>
              </ul>
            </Col>
            <Col className='lincolnLogoCol' sm={4}>
              <img
                className='lincolnLogo'
                src={LincolnLogo}
                alt='Lincoln Logo'
              />
            </Col>
            <Col sm={2}>
              <ul className='footerListItems rightItemsList'>
                <li>Job Opportunities</li>
                <li>Title IX</li>
                <li>Statement</li>
                <li>Security</li>
                <li>Rules and Laws</li>
              </ul>
            </Col>
            <Col sm={2}>
              <ul className='footerListItems lastFooterItemList rightItemsList'>
                <li>Lincoln University</li>
                <li>
                  1570 Baltimore Pike <br /> Lincoln University, PA 19352
                </li>
                <li>678.903.XXXX</li>
                <li>Contact Us</li>
              </ul>
            </Col>
          </Row>
        </Container>
        <div className='footerLine'> </div>
        <Container className='iconsContainer'>
          <Row>
            {mappedIndex(
              (socialMedia, idx) => (
                <div className='socialMediaIconHolder' key={idx}>
                  <a
                    href={socialMedia.src}
                    target='_blank'
                    rel='noopener noreferrer'
                  >
                    <FontAwesomeIcon icon={['fab', socialMedia.icon]} />
                  </a>
                </div>
              ),
              socialMedia
            )}
          </Row>
          <p>&copy; 2019, Lincoln University of Pennsylvania</p>
        </Container>
      </Container>
    );
  }
}

export default PageFooter;
