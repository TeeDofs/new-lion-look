import React from 'react';
import '../../App.scss';
import CarouselView from '../CarouselView/CarouselView';
import HighlightsTab from '../HighlightsTab/HighlightsTab';

import {
  Container,
  Row,
  Col,
  Card,
  CardImg,
  CardBody,
  CardText
} from 'reactstrap';
import ReactPlayer from 'react-player';
import { map, addIndex } from 'ramda';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const mappedIndex = addIndex(map);

class Home extends React.Component {
  render() {
    const {
      carouselImages,
      activeCarouselIndex,
      onCarouselExited,
      onCarouselExiting,
      nextCarouselImage,
      previousCarouselImage,
      goToCarouselIndex,
      upcomingEvents,
      lincolnNewsInfo,
      navTabHighlights,
      navTabOurImpacts,
      activeNavTab,
      toggleNavTab
    } = this.props;
    return (
      <div className='mainBodyContent'>
        <div
          className='carouselDiv'
          style={{
            backgroundImage: `url( ${carouselImages[activeCarouselIndex].src} )`,
            backgroundSize: 'cover',
            backgroundRepeat: 'none',
            backgroundColor: 'rgba(255, 255, 255, 0.8)'
          }}
        ></div>
        <Container className='carouselHolder'>
          <CarouselView
            activeCarouselIndex={activeCarouselIndex}
            carouselImages={carouselImages}
            onCarouselExited={onCarouselExited}
            onCarouselExiting={onCarouselExiting}
            nextCarouselImage={nextCarouselImage}
            previousCarouselImage={previousCarouselImage}
            goToCarouselIndex={goToCarouselIndex}
          />
        </Container>

        <Container className='lincolnNewsHolder' fluid>
          <h2>Lincoln News</h2>
          <Container className='newsCardHolder'>
            <Row>
              {mappedIndex(
                (newsInfo, idx) => (
                  <Col sm='4' key={idx}>
                    <Card className='newsCard'>
                      <CardImg width='100%' height='200px' src={newsInfo.src} />
                      <CardBody>
                        <CardText>{newsInfo.caption}</CardText>
                      </CardBody>
                    </Card>
                  </Col>
                ),
                lincolnNewsInfo
              )}
            </Row>
            <div className='moreNews'>
              <p>
                More News <FontAwesomeIcon icon='chevron-right' />
              </p>
            </div>
          </Container>
        </Container>

        <Container className='videoHolder' fluid>
          <Container>
            <Row>
              <Col sm={8}>
                <div>
                  <ReactPlayer
                    url='https://www.youtube.com/watch?v=XQM2y8GzIJI&t=3s'
                    playing={false}
                    controls={true}
                  />
                </div>
              </Col>
              <Col sm={4}>
                <div className='videoText'>
                  <h4>Explore Lincoln University</h4>
                  <p>
                    Lincoln, also known as the first HBCU is an amazing campus
                    with a lot of bright students and able faculty and staff.
                  </p>
                </div>
              </Col>
            </Row>
          </Container>
        </Container>

        <Container fluid className='highlightsHolder'>
          <Container>
            <HighlightsTab
              navTabHighlights={navTabHighlights}
              navTabOurImpacts={navTabOurImpacts}
              toggleNavTab={toggleNavTab}
              activeNavTab={activeNavTab}
            />
          </Container>
        </Container>

        <Container fluid className='eventsHolder'>
          <Container>
            <h3>Upcoming Events</h3>
            <Row>
              {mappedIndex(
                (event, idx) => (
                  <Col sm={3} key={idx}>
                    <div className='eventDetailsHolder'>
                      <div className='dateHolderDiv'>
                        <Container className='dateHolder'>
                          {event.month}
                          <br />
                          {event.day}{' '}
                        </Container>
                      </div>
                      <div className='eventInfo'>
                        <h5>{event.title}</h5>
                        <div className='eventSummary'>
                          <p>{event.summary}</p>
                        </div>
                      </div>
                      <div className='eventLocationTime'>
                        <p>{event.time}</p>
                        <p>{event.location}</p>
                      </div>
                    </div>
                  </Col>
                ),
                upcomingEvents
              )}
            </Row>
          </Container>
        </Container>
      </div>
    );
  }
}

export default Home;
