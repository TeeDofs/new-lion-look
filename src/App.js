import React from 'react';
import './App.scss';
import NavBar from './components/NavBar/NavBar';
import PageFooter from './components/PageFooter/PageFooter';
import Home from './components/Home/Home';
import About from './components/About/About';
import Admissions from './components/Admissions/Admissions';

import CarouselImage1 from './components/images/lincolnImage1.jpg';
import CarouselImage2 from './components/images/lincolnImage2.jpg';
import CarouselImage3 from './components/images/lincolnImage3.jpg';
import NewsImage1 from './components/images/newsImage1.jpg';
import NewsImage2 from './components/images/newsImage2.jpg';
import NewsImage3 from './components/images/newsImage3.png';
import GraduateImage from './components/images/graduateimage.jpg';
import UndergraduateImage from './components/images/undergraduateimage.jpg';
import BursarImage from './components/images/bursar.png';
import FinancialAidImage from './components/images/fafsaImage.jpg';
import DirectionImage from './components/images/lincolnuniversity1.jpg';

import InternationalImage from './components/images/internationalProgram.jpg';
import AlumniImage from './components/images/lincolnAlumni.jpg';
import ArtsImage from './components/images/lincolnArts.jpg';
import AthleticsImage from './components/images/lincolnAthletics.jpg';
import HistoryImage from './components/images/lincolnHistory.jpg';
import OrganizationImage from './components/images/lincolnOrganization.jpg';

import AboutCarousel1 from './components/images/testImage1.jpg';
import AboutCarousel2 from './components/images/testImage2.jpg';
import AboutCarousel3 from './components/images/ua.png';
import AboutCarousel4 from './components/images/iruma.jpg';

import MLCImage from './components/images/MLCimage.jpg';
import ELCImage from './components/images/ELCimage.jpg';
import ScholarshipImage from './components/images/scholarshipImage.jpg';
import WellnessImage from './components/images/wellnessImage.jpg';
import CommunityServiceImage from './components/images/communityService.jpg';
import FAFSAImage from './components/images/fafsaImage.jpg';

import { Container } from 'reactstrap';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import { slice, find, propEq } from 'ramda';

import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faPaw,
  faSignInAlt,
  faBookReader,
  faThumbsUp,
  faUserGraduate,
  faTrophy,
  faChevronRight,
  faUserShield,
  faUsers,
  faUser,
  faSchool,
  faHistory,
  faBullseye,
  faNewspaper,
  faInfoCircle,
  faStoreAlt,
  faPhoneAlt,
  faDirections,
  faExclamationTriangle,
  faMapMarkedAlt
} from '@fortawesome/free-solid-svg-icons';
import {
  faFacebookSquare,
  faInstagram,
  faTwitter,
  faYoutube,
  faLinkedin,
  faFlickr
} from '@fortawesome/free-brands-svg-icons';
library.add(
  faPaw,
  faSignInAlt,
  faBookReader,
  faThumbsUp,
  faUserGraduate,
  faTrophy,
  faChevronRight,
  faFacebookSquare,
  faInstagram,
  faTwitter,
  faYoutube,
  faLinkedin,
  faFlickr,
  faUserShield,
  faUsers,
  faUser,
  faSchool,
  faHistory,
  faBullseye,
  faNewspaper,
  faInfoCircle,
  faStoreAlt,
  faPhoneAlt,
  faDirections,
  faExclamationTriangle,
  faMapMarkedAlt
);

class App extends React.Component {
  constructor(props) {
    super(props);
    this.toggleNavOption = this.toggleNavOption.bind(this);
    this.setSelectedNavBarOption = this.setSelectedNavBarOption.bind(this);
    this.setSelectedSubNavOption = this.setSelectedSubNavOption.bind(this);
    this.onCarouselExited = this.onCarouselExited.bind(this);
    this.onCarouselExiting = this.onCarouselExiting.bind(this);
    this.nextCarouselImage = this.nextCarouselImage.bind(this);
    this.previousCarouselImage = this.previousCarouselImage.bind(this);
    this.goToCarouselIndex = this.goToCarouselIndex.bind(this);
    this.toggleNavTab = this.toggleNavTab.bind(this);
    this.setClickedNavBarOption = this.setClickedNavBarOption.bind(this);

    this.state = {
      isOpen: false,
      selectedNavBarOption: null,
      clickedNavBarOption: null,
      clickedLink: '/admissions',
      selectedSubNavOption: null,
      activeCarouselIndex: 0,
      activeNavTab: '1',
      lincolnNewsInfo: [
        {
          src: NewsImage1,
          caption:
            'The gathering of the blah blah whatever goes on in here just put something in though'
        },
        {
          src: NewsImage2,
          caption:
            'The Huge Lincoln Career fair attracts a lot of people around the world and turns out to be extremely successful'
        },
        {
          src: NewsImage3,
          caption:
            'The football team comes out with an amazing huge victory after a very long dry spell of some sorts'
        }
      ],
      carouselImages: [
        {
          src: CarouselImage1,
          altText: 'Slide 1',
          caption: 'Slide 1'
        },
        {
          src: CarouselImage2,
          altText: 'Slide 2',
          caption: 'Slide 2'
        },
        {
          src: CarouselImage3,
          altText: 'Slide 3',
          caption: 'Slide 3'
        }
      ],
      navTabHighlights: [
        {
          section: 'International Programs',
          image: InternationalImage
        },
        {
          section: 'Athletics',
          image: AthleticsImage
        },
        {
          section: 'Arts',
          image: ArtsImage
        },
        {
          section: 'History',
          image: HistoryImage
        },
        {
          section: 'Alumni',
          image: AlumniImage
        },
        {
          section: 'Campus Organizations',
          image: OrganizationImage
        }
      ],
      ourImpacts: [
        {
          section: 'English Learning Center',
          image: ELCImage
        },
        {
          section: 'Math Learning Center',
          image: MLCImage
        },
        {
          section: 'Wellness Center',
          image: WellnessImage
        },
        {
          section: 'Scholarships',
          image: ScholarshipImage
        },
        {
          section: 'Community Service',
          image: CommunityServiceImage
        },
        {
          section: 'FAFSA',
          image: FAFSAImage
        }
      ],
      socialMedia: [
        {
          icon: 'facebook-square',
          src: 'https://www.facebook.com/LincolnUniversity/'
        },
        {
          icon: 'twitter',
          src: 'https://twitter.com/LincolnUofPA'
        },
        {
          icon: 'youtube',
          src: 'https://www.youtube.com/user/TheLincolnUniversity'
        },
        {
          icon: 'instagram',
          src: 'https://www.instagram.com/lincolnuniversityofpa/'
        },
        {
          icon: 'linkedin',
          src: 'https://www.linkedin.com/school/lincoln-university/'
        },
        {
          icon: 'flickr',
          src: 'https://www.flickr.com/photos/lincolnuniversity/albums'
        }
      ],
      upcomingEvents: [
        {
          title: 'Freshmen Move In',
          summary:
            'The new freshmen are going to be moving in for a new college experience',
          location: 'The Freshmen Dorms',
          month: 'August',
          day: '17',
          time: '9:00 AM - 9:00 PM'
        },
        {
          title: 'Freshmen Orientation',
          summary:
            "Getting the new freshmen students acquainted with the school, it's rules and regulation",
          location: 'Student Union Building',
          month: 'August',
          day: '19',
          time: '10:00 AM - 4:00 PM'
        },
        {
          title: 'Students Move In',
          summary:
            'Students would be returning to campus after enjoying their various summer breaks',
          location: 'Student Dorms',
          month: 'August',
          day: '23',
          time: '9:00 AM - 9:00 PM'
        },
        {
          title: 'Classes Begin',
          summary:
            'Classes will begin and the students continue their fun and enlightening education process',
          location: 'Classrooms',
          month: 'August',
          day: '26',
          time: '8:00 AM - 6:00 PM'
        }
      ],
      navBarOptions: [
        {
          text: 'About',
          icon: 'paw',
          link: '/about',
          carouselImages: [
            {
              src: AboutCarousel1
            },
            {
              src: AboutCarousel3
            },
            {
              src: AboutCarousel4
            }
          ],
          menu: [
            {
              menuTitle: 'Campus Staff',
              subMenu: [
                {
                  subMenuTitle: 'Administration',
                  icon: 'user-shield',
                  subLinks: [
                    { text: 'Link 1' },
                    { text: 'Link 2' },
                    { text: 'Link 3' },
                    { text: 'Link 4' }
                  ]
                },
                {
                  subMenuTitle: 'Board of Trustees',
                  icon: 'users',
                  subLinks: [
                    { text: 'Link 1' },
                    { text: 'Link 2' },
                    { text: 'Link 3' },
                    { text: 'Link 4' }
                  ]
                },
                {
                  subMenuTitle: 'Human Resources',
                  icon: 'user',
                  subLinks: [
                    { text: 'Link 1' },
                    { text: 'Link 2' },
                    { text: 'Link 3' },
                    { text: 'Link 4' }
                  ]
                }
              ]
            },
            {
              menuTitle: 'Campus Facts',
              subMenu: [
                {
                  subMenuTitle: 'Alma Mater',
                  icon: 'school',
                  subLinks: [
                    { text: 'Link 1' },
                    { text: 'Link 2' },
                    { text: 'Link 3' },
                    { text: 'Link 4' }
                  ]
                },
                {
                  subMenuTitle: 'History',
                  icon: 'history',
                  subLinks: [
                    { text: 'Link 1' },
                    { text: 'Link 2' },
                    { text: 'Link 3' },
                    { text: 'Link 4' }
                  ]
                },
                {
                  subMenuTitle: 'Mission',
                  icon: 'bullseye',
                  subLinks: [
                    { text: 'Link 1' },
                    { text: 'Link 2' },
                    { text: 'Link 3' },
                    { text: 'Link 4' }
                  ]
                },
                {
                  subMenuTitle: 'News & Events',
                  icon: 'newspaper',
                  subLinks: [
                    { text: 'Link 1' },
                    { text: 'Link 2' },
                    { text: 'Link 3' },
                    { text: 'Link 4' }
                  ]
                },
                {
                  subMenuTitle: 'University Facts',
                  icon: 'info-circle',
                  subLinks: [
                    { text: 'Link 1' },
                    { text: 'Link 2' },
                    { text: 'Link 3' },
                    { text: 'Link 4' }
                  ]
                }
              ]
            },
            {
              menuTitle: 'Campus Navigation',
              subMenu: [
                {
                  subMenuTitle: 'Campus Store',
                  icon: 'store-alt',
                  subLinks: [
                    { text: 'Link 1' },
                    { text: 'Link 2' },
                    { text: 'Link 3' },
                    { text: 'Link 4' }
                  ]
                },
                {
                  subMenuTitle: 'Contact Us',
                  icon: 'phone-alt',
                  subLinks: [
                    { text: 'Link 1' },
                    { text: 'Link 2' },
                    { text: 'Link 3' },
                    { text: 'Link 4' }
                  ]
                },
                {
                  subMenuTitle: 'Directions to Campus',
                  icon: 'directions',
                  subLinks: [
                    { text: 'Link 1' },
                    { text: 'Link 2' },
                    { text: 'Link 3' },
                    { text: 'Link 4' }
                  ]
                },
                {
                  subMenuTitle: 'Emergency Info',
                  icon: 'exclamation-triangle',
                  subLinks: [
                    { text: 'Link 1' },
                    { text: 'Link 2' },
                    { text: 'Link 3' },
                    { text: 'Link 4' }
                  ]
                },
                {
                  subMenuTitle: 'Maps',
                  icon: 'map-marked-alt',
                  subLinks: [
                    { text: 'Link 1' },
                    { text: 'Link 2' },
                    { text: 'Link 3' },
                    { text: 'Link 4' }
                  ]
                }
              ]
            }
          ]
        },
        {
          text: 'Admissions',
          icon: 'sign-in-alt',
          link: '/admissions',
          carouselImages: [
            {
              src: AboutCarousel1
            },
            {
              src: AboutCarousel3
            },
            {
              src: AboutCarousel4
            }
          ],
          menu: [
            {
              menuTitle: 'Undergraduate Admissions',
              image: UndergraduateImage,
              subMenu: [
                { subMenuTitle: 'International Students', icon: 'paw' },
                { subMenuTitle: 'Transfer Students', icon: 'paw' },
                {
                  subMenuTitle: 'Undergraduate Application Requirements',
                  icon: 'paw'
                },
                { subMenuTitle: 'Undergraduate Students', icon: 'paw' },
                { subMenuTitle: 'Undergraduate Programs', icon: 'paw' }
              ]
            },
            {
              menuTitle: 'Graduate Admissions',
              image: GraduateImage,
              subMenu: [
                {
                  subMenuTitle: 'Graduate Application Requirements',
                  icon: 'paw'
                },
                { subMenuTitle: 'Graduate Students', icon: 'paw' },
                { subMenuTitle: 'Graduate Programs', icon: 'paw' },
                { subMenuTitle: 'Mission', icon: 'paw' },
                { subMenuTitle: 'News & Events', icon: 'paw' },
                { subMenuTitle: 'University Facts', icon: 'paw' }
              ]
            },
            {
              menuTitle: 'Bursar',
              image: BursarImage,
              subMenu: [
                { subMenuTitle: 'Admitted Students', icon: 'paw' },
                { subMenuTitle: 'Bursar Forms', icon: 'paw' },
                { subMenuTitle: 'Bursar FAQS', icon: 'paw' },
                {
                  subMenuTitle: '1098 T Federal Education Credit',
                  icon: 'paw'
                },
                { subMenuTitle: 'Faculty & Staff', icon: 'paw' },
                { subMenuTitle: 'Web Advisor', icon: 'paw' }
              ]
            },
            {
              menuTitle: 'Financial Aid',
              image: FinancialAidImage,
              subMenu: [
                { subMenuTitle: 'Academic Progress', icon: 'paw' },
                { subMenuTitle: 'Cost of Attendance', icon: 'paw' },
                { subMenuTitle: 'Faculty & Staff', icon: 'paw' },
                { subMenuTitle: 'Financial Aid Forms', icon: 'paw' },
                {
                  subMenuTitle: 'Policy on Satisfactory Academic Progress',
                  icon: 'paw'
                },
                {
                  subMenuTitle: 'Student Responsibilities and Repayment',
                  icon: 'paw'
                },
                {
                  subMenuTitle: 'Repayment, Default & Debt Management',
                  icon: 'paw'
                },
                { subMenuTitle: 'Web Advisor', icon: 'paw' }
              ]
            },
            {
              menuTitle: 'Campus Location',
              image: DirectionImage,
              subMenu: [
                { subMenuTitle: 'Main Campus', icon: 'paw' },
                { subMenuTitle: 'University City', icon: 'paw' },
                { subMenuTitle: 'Directions to Campus', icon: 'paw' },
                { subMenuTitle: 'Maps', icon: 'paw' }
              ]
            }
          ]
        },
        {
          text: 'Academics',
          icon: 'book-reader',
          link: '/academics',
          carouselImages: [
            {
              src: AboutCarousel1
            },
            {
              src: AboutCarousel2
            }
          ],
          menu: [
            {
              menuTitle: 'Campus Staff',
              subMenu: [
                { subMenuTitle: 'Administration', icon: 'user-shield' },
                { subMenuTitle: 'Board of Trustees', icon: 'paw' },
                { subMenuTitle: 'Human Resources', icon: 'paw' }
              ]
            },
            {
              menuTitle: 'Campus Facts',
              subMenu: [
                { subMenuTitle: 'Alma Mater', icon: 'paw' },
                { subMenuTitle: 'History', icon: 'paw' },
                { subMenuTitle: 'Mission', icon: 'paw' },
                { subMenuTitle: 'News & Events', icon: 'paw' },
                { subMenuTitle: 'University Facts', icon: 'paw' }
              ]
            },
            {
              menuTitle: 'Campus Navigation',
              subMenu: [
                { subMenuTitle: 'Campus Store', icon: 'paw' },
                { subMenuTitle: 'Contact Us', icon: 'paw' },
                { subMenuTitle: 'Directions to Campus', icon: 'paw' },
                { subMenuTitle: 'Emergency Info', icon: 'paw' },
                { subMenuTitle: 'Maps', icon: 'paw' }
              ]
            }
          ]
        },
        {
          text: 'Student Success',
          icon: 'thumbs-up',
          link: '/student-success',
          carouselImages: [
            {
              src: AboutCarousel1
            },
            {
              src: AboutCarousel2
            }
          ],
          menu: [
            {
              menuTitle: 'Campus Staff',
              subMenu: [
                { subMenuTitle: 'Administration', icon: 'user-shield' },
                { subMenuTitle: 'Board of Trustees', icon: 'paw' },
                { subMenuTitle: 'Human Resources', icon: 'paw' }
              ]
            },
            {
              menuTitle: 'Campus Facts',
              subMenu: [
                { subMenuTitle: 'Alma Mater', icon: 'paw' },
                { subMenuTitle: 'History', icon: 'paw' },
                { subMenuTitle: 'Mission', icon: 'paw' },
                { subMenuTitle: 'News & Events', icon: 'paw' },
                { subMenuTitle: 'University Facts', icon: 'paw' }
              ]
            },
            {
              menuTitle: 'Campus Navigation',
              subMenu: [
                { subMenuTitle: 'Campus Store', icon: 'paw' },
                { subMenuTitle: 'Contact Us', icon: 'paw' },
                { subMenuTitle: 'Directions to Campus', icon: 'paw' },
                { subMenuTitle: 'Emergency Info', icon: 'paw' },
                { subMenuTitle: 'Maps', icon: 'paw' }
              ]
            }
          ]
        },
        {
          text: 'Athletics',
          icon: 'trophy',
          link: '/athletics',
          carouselImages: [
            {
              src: AboutCarousel1
            },
            {
              src: AboutCarousel2
            }
          ],
          menu: [
            {
              menuTitle: 'Campus Staff',
              subMenu: [
                { subMenuTitle: 'Administration', icon: 'user-shield' },
                { subMenuTitle: 'Board of Trustees', icon: 'paw' },
                { subMenuTitle: 'Human Resources', icon: 'paw' }
              ]
            },
            {
              menuTitle: 'Campus Facts',
              subMenu: [
                { subMenuTitle: 'Alma Mater', icon: 'paw' },
                { subMenuTitle: 'History', icon: 'paw' },
                { subMenuTitle: 'Mission', icon: 'paw' },
                { subMenuTitle: 'News & Events', icon: 'paw' },
                { subMenuTitle: 'University Facts', icon: 'paw' }
              ]
            },
            {
              menuTitle: 'Campus Navigation',
              subMenu: [
                { subMenuTitle: 'Campus Store', icon: 'paw' },
                { subMenuTitle: 'Contact Us', icon: 'paw' },
                { subMenuTitle: 'Directions to Campus', icon: 'paw' },
                { subMenuTitle: 'Emergency Info', icon: 'paw' },
                { subMenuTitle: 'Maps', icon: 'paw' }
              ]
            }
          ]
        },
        {
          text: 'Alumni',
          icon: 'user-graduate',
          link: '/alumni',

          carouselImages: [
            {
              src: AboutCarousel1
            },
            {
              src: AboutCarousel2
            }
          ],
          menu: [
            {
              menuTitle: 'Campus Staff',
              subMenu: [
                { subMenuTitle: 'Administration', icon: 'user-shield' },
                { subMenuTitle: 'Board of Trustees', icon: 'paw' },
                { subMenuTitle: 'Human Resources', icon: 'paw' }
              ]
            },
            {
              menuTitle: 'Campus Facts',
              subMenu: [
                { subMenuTitle: 'Alma Mater', icon: 'paw' },
                { subMenuTitle: 'History', icon: 'paw' },
                { subMenuTitle: 'Mission', icon: 'paw' },
                { subMenuTitle: 'News & Events', icon: 'paw' },
                { subMenuTitle: 'University Facts', icon: 'paw' }
              ]
            },
            {
              menuTitle: 'Campus Navigation',
              subMenu: [
                { subMenuTitle: 'Campus Store', icon: 'paw' },
                { subMenuTitle: 'Contact Us', icon: 'paw' },
                { subMenuTitle: 'Directions: Getting to Campus', icon: 'paw' },
                { subMenuTitle: 'Emergency Info', icon: 'paw' },
                { subMenuTitle: 'Maps', icon: 'paw' }
              ]
            }
          ]
        }
      ]
    };
  }

  componentDidMount() {
    if (slice(21, Infinity, window.location.href) !== '/') {
      const currentNavOption = find(
        propEq('link', slice(21, Infinity, window.location.href))
      )(this.state.navBarOptions).text;
      this.setState({
        clickedNavBarOption: currentNavOption,
        clickedLink: slice(21, Infinity, window.location.href)
      });
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.clickedLink !== this.state.clickedLink) {
      this.setState({
        clickedNavBarOption: find(
          propEq('link', slice(21, Infinity, window.location.href))
        )(this.state.navBarOptions).text
      });
    }
  }

  //Navbar functions
  toggleNavOption() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  setSelectedNavBarOption(navBarOption) {
    this.setState({
      selectedNavBarOption: navBarOption
    });
  }

  setClickedNavBarOption(option) {
    this.setState({
      clickedNavBarOption: option
    });
  }

  setSelectedSubNavOption(subNavOption) {
    this.setState({
      selectedSubNavOption: subNavOption
    });
  }

  //Carousel functions
  onCarouselExiting() {
    this.animating = true;
  }

  onCarouselExited() {
    this.animating = false;
  }

  nextCarouselImage() {
    if (this.animating) return;
    const nextIndex =
      this.state.activeCarouselIndex === this.state.carouselImages.length - 1
        ? 0
        : this.state.activeCarouselIndex + 1;
    this.setState({ activeCarouselIndex: nextIndex });
  }

  previousCarouselImage() {
    if (this.animating) return;
    const nextIndex =
      this.state.activeCarouselIndex === 0
        ? this.state.carouselImages.length - 1
        : this.state.activeCarouselIndex - 1;
    this.setState({ activeCarouselIndex: nextIndex });
  }

  goToCarouselIndex(newIndex) {
    if (this.animating) return;
    this.setState({ activeCarouselIndex: newIndex });
  }

  //Highlights Nav Tab Functions
  toggleNavTab(tab) {
    if (this.state.activeNavTab !== tab) {
      this.setState({
        activeNavTab: tab
      });
    }
  }

  render() {
    return (
      <Router>
        <div className='App'>
          <div className='appBody'>
            <Container className='navBarHolder' fluid>
              <NavBar
                toggleNavOption={this.toggleNavOption}
                navOptionIsOpen={this.state.isOpen}
                navBarOptions={this.state.navBarOptions}
                setSelectedNavBarOption={this.setSelectedNavBarOption}
                selectedNavBarOption={this.state.selectedNavBarOption}
                setSelectedSubNavOption={this.setSelectedSubNavOption}
                selectedSubNavOption={this.state.selectedSubNavOption}
                clickedNavBarOption={this.state.clickedNavBarOption}
                setClickedNavBarOption={this.setClickedNavBarOption}
              />
            </Container>

            <Switch>
              <Route
                path='/'
                exact
                render={props => (
                  <Home
                    activeCarouselIndex={this.state.activeCarouselIndex}
                    carouselImages={this.state.carouselImages}
                    onCarouselExited={this.onCarouselExited}
                    onCarouselExiting={this.onCarouselExiting}
                    nextCarouselImage={this.nextCarouselImage}
                    previousCarouselImage={this.previousCarouselImage}
                    goToCarouselIndex={this.goToCarouselIndex}
                    upcomingEvents={this.state.upcomingEvents}
                    lincolnNewsInfo={this.state.lincolnNewsInfo}
                    navTabHighlights={this.state.navTabHighlights}
                    navTabOurImpacts={this.state.ourImpacts}
                    activeNavTab={this.state.activeNavTab}
                    toggleNavTab={this.toggleNavTab}
                  />
                )}
              />

              <Route
                path='/about'
                render={props => (
                  <About
                    activeCarouselIndex={this.state.activeCarouselIndex}
                    onCarouselExited={this.onCarouselExited}
                    onCarouselExiting={this.onCarouselExiting}
                    nextCarouselImage={this.nextCarouselImage}
                    previousCarouselImage={this.previousCarouselImage}
                    goToCarouselIndex={this.goToCarouselIndex}
                    navBarOptions={this.state.navBarOptions}
                  />
                )}
              />

              <Route
                path='/admissions'
                render={props => (
                  <Admissions
                    activeCarouselIndex={this.state.activeCarouselIndex}
                    onCarouselExited={this.onCarouselExited}
                    onCarouselExiting={this.onCarouselExiting}
                    nextCarouselImage={this.nextCarouselImage}
                    previousCarouselImage={this.previousCarouselImage}
                    goToCarouselIndex={this.goToCarouselIndex}
                    navBarOptions={this.state.navBarOptions}
                  />
                )}
              />
            </Switch>

            <div className='footerHolder'>
              <PageFooter socialMedia={this.state.socialMedia} />
            </div>
          </div>
        </div>
      </Router>
    );
  }
}

export default App;
